<!-- BEGIN JAVASCRIPT -->
<script src="<?php echo base_url(); ?>/assets/js/libs/jquery/jquery-1.11.2.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/libs/jquery/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/libs/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/libs/spin.js/spin.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/libs/autosize/jquery.autosize.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/libs/moment/moment.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/libs/flot/jquery.flot.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/libs/flot/jquery.flot.time.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/libs/flot/jquery.flot.resize.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/libs/flot/jquery.flot.orderBars.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/libs/flot/jquery.flot.pie.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/libs/flot/curvedLines.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/libs/jquery-knob/jquery.knob.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/libs/sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/libs/nanoscroller/jquery.nanoscroller.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/libs/d3/d3.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/libs/d3/d3.v3.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/libs/rickshaw/rickshaw.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/core/source/App.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/core/source/AppNavigation.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/core/source/AppOffcanvas.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/core/source/AppCard.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/core/source/AppForm.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/core/source/AppNavSearch.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/core/source/AppVendor.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/core/demo/Demo.js"></script>
<script type="text/javascript" src="http://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="http://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/jquery.fileuploadmulti.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/custom_listing.js"></script>
<!-- END JAVASCRIPT -->
</body>

<!-- PAGE LEVEL FOOTER SCRIPTS -->
<script src="<?php echo base_url(); ?>theme/js/jquery.bootstrap.wizard.js"></script>
<?php if (isset($includes_for_layout_js['js']) AND count($includes_for_layout_js['js']) > 0): ?>
    <?php foreach ($includes_for_layout_js['js'] as $js): ?>
        <?php if ($js['options'] === NULL OR $js['options'] == 'footer'): ?>
            <script type="text/javascript" src="<?php echo $js['file']; ?>"></script>
        <?php endif; ?>	
    <?php endforeach; ?>
<?php endif; ?>

<!-- END PAGE LEVEL  FOOTER SCRIPTS -->

<!-- Footer Google Analytics Code 
<script>
<?php
$analyticsCode = getGoogleAnalyticCode('google_analytic_code');
if ($analyticsCode)
    echo $analyticsCode;
?>
</script>-->
</html>