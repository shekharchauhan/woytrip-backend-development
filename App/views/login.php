<?php $this->load->view('admin/header'); ?>

<!-- BEGIN BASE-->
<div id="base">

    <!-- BEGIN OFFCANVAS LEFT -->
    <div class="offcanvas">
    </div><!--end .offcanvas-->
    <!-- END OFFCANVAS LEFT -->

    <!-- BEGIN CONTENT-->
    <div id="content">
        <?php $this->load->view($main_content); ?>
    </div><!--end #content-->
    <!-- END CONTENT -->

    <!-- BEGIN OFFCANVAS RIGHT -->
    <div class="offcanvas">
        <!-- BEGIN OFFCANVAS SEARCH -->
        <?php $this->load->view('admin/chat'); ?>
        <!-- END OFFCANVAS SEARCH -->
    </div><!--end .offcanvas-->


</div><!--end #base-->
<!-- END BASE -->

<?php $this->load->view('admin/footer'); ?>
