<?php

/* This function is used to include the CSS and JS (Sanjay Yadav) */
if (! function_exists ( 'add_includes' )) {
	function add_includes($type, $files, $options = NULL, $prepend_base_url = TRUE) {
		$ci = &get_instance ();
		$includes = array ();
		if (is_array ( $files ) and ! empty ( $files )) {
			foreach ( $files as $file ) {
				if ($prepend_base_url) {
					$ci->load->helper ( 'url' );
					$file = base_url () . $file;
				}
				
				$includes [$type] [] = array (
						'file' => $file,
						'options' => $options 
				);
			}
		} else {
			if ($prepend_base_url) {
				$ci->load->helper ( 'url' );
				$file = base_url () . $files;
			}
			
			$includes [$type] [] = array (
					'file' => $file,
					'options' => $options 
			);
		}
		
		return $includes;
	}
}

/* End Of function add_includes */

if (! function_exists ( 'getActiveMenu' )) {
	function getActiveMenu($url) {
		$CI = &get_instance ();
		$alias = $CI->uri->segment ( 2 );
		$mfl = $CI->router->fetch_class ();
		$mthd = $CI->router->fetch_method ();
		if ($mfl == $url) {
			$class = "active";
		} else {
			$class = "";
		}
		return $class;
	}
}

if (! function_exists ( 'check_auth' )) {
	function check_auth() {
		$CI = &get_instance ();
		$CI->load->module ( "wo_users" );
		// if (!$CI->users->_is_admin()) {
		if (! $CI->users->userdata ()) {
			redirect ( '' );
		}
	}
}

if (! function_exists ( 'site_title' )) {
	function site_title() {
		$CI = &get_instance ();
		$CI->load->module ( "cms" );
		$title = $CI->cms->getSitetitle ();
		$title = @unserialize ( $title );
		return $title ['c_title'];
	}
}
if (! function_exists ( 'site_logo' )) {
	function site_logo() {
		$CI = &get_instance ();
		$CI->db->where ( 'alias', 'logo' );
		$query = $CI->db->get ( 'cms' );
		return $query->row ()->content;
	}
}
if (! function_exists ( 'state_name' )) {
	function state_name($id) {
		$CI = &get_instance ();
		$CI->db->where ( 'id', $id );
		$query = $CI->db->get ( 'state' );
		if ($query->num_rows () > 0)
			return $query->row ()->state_name;
	}
}
if (! function_exists ( 'country_name' )) {
	function country_name($id) {
		$CI = &get_instance ();
		$CI->db->where ( 'country_code', $id );
		$query = $CI->db->get ( 'country' );
		if ($query->num_rows () > 0)
			return $query->row ()->country_name;
	}
}

if (! function_exists ( 'getGoogleAnalyticCode' )) {
	function getGoogleAnalyticCode($alias) {
		$CI = &get_instance ();
		$CI->db->start_cache ();
		$CI->db->where ( 'alias', $alias );
		$query = $CI->db->get ( 'cms' );
		$CI->db->flush_cache ();
		$qqq = $query->row ();
		$yourCode = array ();
		$yourCode ['content'] = unserialize ( $qqq->content );
		return $yourCode ['content'] ['description'];
	}
}

if (! function_exists ( 'getPages' )) {
	function getPages($slug = null) {
		$CI = &get_instance ();
		$CI->db->start_cache ();
		$con = array (
				'include_in' => $slug,
				'is_active' => '1',
				'is_deleted' => '0' 
		);
		$CI->db->where ( $con );
		$query = $CI->db->get ( 'pages' );
		$CI->db->flush_cache ();
		$qqq = $query->result ();
		return $qqq;
	}
}

if (! function_exists ( 'state_codes' )) {
	function state_codes() {
		$CI = &get_instance ();
		$query = $CI->db->get ( 'state' );
		$states = $query->result ();
		$data = array ();
		foreach ( $states as $list ) {
			$data [strtolower ( $list->state_name )] = $list->id;
		}
		return $data;
	}
}

if (! function_exists ( 'site_limit' )) {
	function site_limit() {
		$CI = &get_instance ();
		$CI->load->module ( "cms" );
		$title = $CI->cms->getSitetitle ();
		$title = @unserialize ( $title );
		return $title ['c_limit'];
	}
}
function admin_base_url($uri = '') {
	$CI = & get_instance ();
	return $CI->config->base_url ( ADMIN . '/' . $uri );
}
function admin_url($uri = '') {
	$CI = & get_instance ();
	return $CI->config->site_url ( ADMIN . '/' . $uri );
}
function admin_redirect($uri = '', $method = 'location', $http_response_code = 302) {
	if (! preg_match ( '#^https?://#i', $uri )) {
		$uri = admin_url ( $uri );
	}
	
	switch ($method) {
		case 'refresh' :
			header ( "Refresh:0;url=" . $uri );
			break;
		default :
			header ( "Location: " . $uri, TRUE, $http_response_code );
			break;
	}
	exit ();
}

$seoURL = '';
function base_url_without_slash() {
	return substr ( base_url (), 0, strlen ( base_url () ) - 1 );
}
function getHref($menu_id) {
	global $seoURL;
	$seoURL = '';
	return gethyperlink ( $menu_id );
}
function gethyperlink($menu_id) {
	global $seoURL;
	$href = '';
	$hrefRw = '';
	
	$ci = & get_instance ();
	$ci->load->database ();
	
	if (is_int ( $menu_id ) || $menu_id != '') {
		$query = $ci->db->query ( "SELECT * FROM menu WHERE menu_id = $menu_id AND menu_status = 'Active'" );
		if ($query->num_rows () > 0) {
			$hrefRw = $query->row_array ();
			if (strtolower ( $hrefRw ['menu_name'] ) == 'home') {
				return $href = " href = '" . base_url () . "'";
			} else if ($hrefRw ['menu_page_type'] == 'Page') {
				$seoURL = "/" . $hrefRw ['menu_slug'] . $seoURL;
				gethyperlink ( $hrefRw ['menu_parent_id'] );
			} else {
				if (! strstr ( $hrefRw ['menu_link'], "http://" ) && ! strstr ( $hrefRw ['menu_link'], "https://" )) {
					if (! strstr ( $hrefRw ['menu_link'], "http://" )) {
						$href = " href = 'http://" . $hrefRw ['menu_link'] . "'";
					}
				} else {
					$href = " href = '" . $hrefRw ['menu_link'] . "'";
				}
				if ($hrefRw ['menu_link_type'] == '_blank') {
					$href .= " target = '_blank'";
				}
			}
		}
		if (isset ( $hrefRw ['menu_page_type'] )) {
			if ($hrefRw ['menu_page_type'] == 'Page') {
				$href = " href = '" . base_url_without_slash () . $seoURL . ".html'";
			}
		}
		return $href;
	} else {
		return false;
	}
}
function set_pagination() {
	$CI = &get_instance ();
	$CI->page_config ['full_tag_open'] = '<div class="pagination pagination-small pagination-right"><ul class="pagination">';
	$CI->page_config ['full_tag_close'] = '</ul></div>';
	$CI->page_config ['first_link'] = true;
	$CI->page_config ['last_link'] = true;
	$CI->page_config ['first_tag_open'] = '<li>';
	$CI->page_config ['first_tag_close'] = '</li>';
	$CI->page_config ['prev_link'] = '&laquo';
	$CI->page_config ['prev_tag_open'] = '<li class="prev">';
	$CI->page_config ['prev_tag_close'] = '</li>';
	$CI->page_config ['next_link'] = '&raquo';
	$CI->page_config ['next_tag_open'] = '<li>';
	$CI->page_config ['next_tag_close'] = '</li>';
	$CI->page_config ['last_tag_open'] = '<li>';
	$CI->page_config ['last_tag_close'] = '</li>';
	$CI->page_config ['cur_tag_open'] = '<li class="active"><a href="#">';
	$CI->page_config ['cur_tag_close'] = '</a></li>';
	$CI->page_config ['num_tag_open'] = '<li>';
	$CI->page_config ['num_tag_close'] = '</li>';
	return $CI->page_config;
}
function addhttp($url) {
	if (! preg_match ( "~^(?:f|ht)tps?://~i", $url )) {
		$url = "http://" . $url;
	}
	return $url;
}

if (! function_exists ( 'getUserDeviceIDs' )) {
	function getUserDeviceIDs($userIDs = array(), $deviceType = '') {
		
		//print_r($userIDs); die();
		//echo 9898; die();
		$CI = &get_instance ();
		$CI->db->select ( 'd.user_id,d.reg_id,d.fcm_id,u.name' );
		if ($deviceType == 'ios') :
			$CI->db->where ( "device_type", 'ios' );
		 else :
			$CI->db->where ( "device_type", 'android' );
		endif;
		if (! empty ( $userIDs ))
			$CI->db->where_in ( 'd.user_id', $userIDs );
		$CI->db->from ( 'wo_user_devices d' );
		$CI->db->join ( 'wo_users u', 'u.user_id=d.user_id', 'left' );
		$query = $CI->db->get (); //echo $this->db->last_query(); die();
		$getuser = $query->result_array ();
		return $getuser;
	}
}

if (! function_exists ( 'getUserDeviceIDs2' )) {
	function getUserDeviceIDs2($userIDs, $deviceType = '') {
		$CI = &get_instance ();
		$CI->db->select ( 'd.user_id,d.reg_id,d.fcm_id,u.name' );
		if ($deviceType == 'ios') :
			$CI->db->where ( "device_type", 'ios' );
		 else :
			$CI->db->where ( "device_type", 'android' );
		endif;
		if (! empty ( $userIDs ))
			$CI->db->where_in ( 'd.user_id', $userIDs );
		$CI->db->from ( 'wo_user_devices d' );
		$CI->db->join ( 'wo_users u', 'u.user_id=d.user_id', 'left' );
		$query = $CI->db->get ();
		$getuser = $query->result_array ();
		return $getuser;
	}
}

if (! function_exists ( 'createNotification' )) {
	function createNotification($data = array()) {
		$CI = &get_instance ();
		$create = $CI->db->insert_batch ( 'wo_notifications', $data );
		if ($create) {
			return true;
		} else {
			return false;
		}
	}
}

/*
 * Author : Gaurav Ranjan
 * 
 * Description : This Funtion will handle PushNotifications.
 * 
 * 
 */

if (! function_exists ( 'androidNotification' )) {
	function androidNotification($deviceIDS, $data = '') {
		
		// print_r($data); die();
		$CI = &get_instance ();
		$getuser = $deviceIDS;
		$notificationArray = array ();
		foreach ( $getuser as $val ) {
			$register_send = $val;
			$notification = array (
					'user_id' => $data ['user_id'],
					'subject_type' => $data ['subject_type'],
					'subject_id' => $data ['subject_id'],
					'object_type' => 'user',
					'object_id' => $register_send ['user_id'],
					'read' => '0',
					'params' => json_encode ( array (
							'title' => $data ['title'],
							'message' => $data ['message'] 
					) ),
					'date' => date ( 'Y-m-d H:i:s' ) 
			);
			array_push ( $notificationArray, $notification );
			$value = array (
					$register_send ['fcm_id'] 
			);
			$get = $value;
			$message = $data ['message'];
			$title = $data ['title'];
			$notiType = "force";
			$api_key = 'AIzaSyAaSoFQVQWoPapbQZe5wb2DTEu2hQYxcdY';
			
			if (empty ( $api_key ) || count ( $get ) < 0) {
				$result = array (
						'success' => '0',
						'message' => 'api or reg id not found' 
				);
				echo json_encode ( $result );
				die ();
			}
			$registrationIDs = $get;
			$url = 'https://fcm.googleapis.com/fcm/send';
			$fields = array (
					'registration_ids' => $registrationIDs,
					'data' => $data 
			);
			// 'notification' => array("title" => $title, "body" => $message),
			
			
			$headers = array (
					'Authorization: key=' . $api_key,
					'Content-Type: application/json' 
			);
			
			$ch = curl_init ();
			curl_setopt ( $ch, CURLOPT_URL, $url );
			curl_setopt ( $ch, CURLOPT_POST, true );
			curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
			curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode ( $fields ) );
			$result = curl_exec ( $ch );
			
			// print_r($result);
			
			curl_close ( $ch );
		}
		createNotification ( $notificationArray );
	}
}
function get_field_value($tablename,$field,$where_in,$where_val){
	$CI      =& get_instance();
	$result = $CI->db->query("select $field from $tablename where $where_in='$where_val'");
	return $result;
}
function resultByQuery($query){
	$CI      =& get_instance();
	$result = $CI->db->query($query);
	return $result;
}
?>