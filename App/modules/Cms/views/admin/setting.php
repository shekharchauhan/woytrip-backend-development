<?php
$aBlock = $this->input->get('S') ? $this->input->get('S') : 'contacts';
$Tabs = array(
    'contacts' => 'Contacts',
    'google_analytic_code' => 'Google Analytic Code',
    'logo' => 'Site Logo'
);
?>
<section>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-md-12">
                <div class="card-head style-primary">
                    <header>Settings</header>
                </div>
                <div class="alert alert-success alert-dismissable" id="successMsg" style="display:<?php echo @$update == true ? 'block' : 'none'; ?>;">
                    Successfully Updated . .<button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="md md-close pull-right"></i></button>                               
                </div>
                <div class="card tabs-left style-default-light">
                    <ul class="card-head nav nav-tabs text-center" data-toggle="tabs">
                        <?php foreach ($Tabs as $t => $abs): ?>				    
                            <li class="<?php echo $aBlock == $t ? 'active' : ''; ?>">
                                <a data-toggle="tab" href="#<?php echo $t; ?>"><i class="fa fa-lg fa-user"></i><br/><h4><?php echo $abs; ?></h4></a>
                            </li>												
                        <?php endforeach; ?>
                    </ul>
                    <?php $contacts = @unserialize($content['contacts']->content); ?>
                    <?php
                    $contArray = array(
                        'c_telephone' => 'Customer Service Telephone',
                        'c_email' => 'Customer Service E-mail',
                        'c_fax' => 'Fax',
                        'c_address' => 'Address',
                        'c_facebook' => 'Facebook',
                        'c_title' => 'Site Title',
                        'c_limit' => 'Limit Per Page'
                    );
                    ?>
                    <div class="card-body tab-content style-default-bright">
                        <div class="tab-pane <?php echo $aBlock == 'contacts' ? ' active' : ''; ?>" id="contacts">
                            <form action="<?php echo base_url('admin/cms/setting?S=contacts'); ?>" method="post" role="form">
                                <div class="col-sm-12 col-md-12">
                                    <?php foreach ($contArray as $name => $title): ?>
                                        <div class="form-group">
                                            <label for="<?php echo $name; ?>"><?php echo $title; ?></label>
                                            <input type="test" class="form-control" id="<?php echo $name; ?>" name="<?php echo $name; ?>" value="<?php echo @$contacts[$name]; ?>">
                                        </div>
                                        <div class="clearfix"></div>  
                                    <?php endforeach; ?>
                                </div>
                                <div class="clear" style="clear:both;"></div>
                                <div class="col-sm-12 col-md-12" style="text-align:center;">
                                    <button type="submit" class="btn ink-reaction btn-raised btn-primary btn-loading-state" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading...">Save<div style="top: 26px; left: 32.5px;" class="ink"></div></button>
                                </div>
                                <input type="hidden" name="section" value="contacts" />
                            </form>
                        </div>
                        <?php
                        $simpleTabs = array(
                            'google_analytic_code' => 'Google Analytic Code',
                        );
                        ?>
                        <?php foreach ($simpleTabs as $tabkey => $tabs): ?>
                            <div class="tab-pane fade<?php echo $aBlock == $tabkey ? ' in active' : ''; ?>" id="<?php echo $tabkey; ?>">
                                <form action="<?php echo base_url('admin/cms/setting?S=' . $tabkey); ?>" method="post">
                                    <?php $cntnt = @unserialize($content[$tabkey]->content); ?>
                                    <div class="col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <label>Title</label>
                                            <input type="text" class="form-control" name="title" value="<?php echo @$cntnt['title']; ?>" />
                                        </div>
                                    </div>

                                    <div class="col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea rows="10" class="form-control" name="description" ><?php echo @$cntnt['description']; ?></textarea>
                                        </div>
                                    </div>

                                    <div class="clear" style="clear:both;"></div>
                                    <div class="col-sm-12 col-md-12" style="text-align:center;">
                                        <button type="submit" class="btn ink-reaction btn-raised btn-primary btn-loading-state" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading...">Save<div style="top: 26px; left: 32.5px;" class="ink"></div></button>
                                    </div>
                                    <input type="hidden" name="section" value="<?php echo $tabkey; ?>" />						            
                                </form>
                            </div>
                        <?php endforeach; ?>
                        <div class="tab-pane fade<?php echo $aBlock == 'logo' ? ' in active' : ''; ?>" id="logo">
                            <form class="form-horizontal" action="<?php echo base_url('admin/cms/setting?S=logo'); ?>" method="post" enctype="multipart/form-data">
                                <?php if ($content['logo']->content && file_exists(IMAGESPATH . 'logo/' . $content['logo']->content)) { ?>
                                    <div class="form-group">
                                        <label class="control-label">Current Logo</label>
                                        <div class="col-sm-12 col-md-12">
                                            <img src="<?php echo base_url('uploads/logo/' . $content['logo']->content); ?>" width="140" />
                                            <input type="checkbox" id="deleteLogo" name="deleteLogo" value="1" />Delete
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Site Logo</label>
                                        <div class="col-sm-12 col-md-12">
                                            <input type="file" class="form-control" name="logo" />
                                        </div>
                                    </div>
                                </div>
                                <div class="clear" style="clear:both;"></div>
                                <div class="col-sm-12 col-md-12" style="text-align:center;">
                                    <button type="submit" class="btn ink-reaction btn-raised btn-primary btn-loading-state" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading...">Save<div style="top: 26px; left: 32.5px;" class="ink"></div></button>
                                </div>
                                <input type="hidden" name="section" value="logo" />
                            </form>
                        </div>
                    </div>
                </div><!--end .card -->
                <em class="text-caption">Setting</em>
            </div><!--end .col -->
        </div>
    </div>
</section>
