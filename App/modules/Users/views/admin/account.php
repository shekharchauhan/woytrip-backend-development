<style>
#offcanvas-search{display:none;}
</style>

<section>
    <div class="contain-lg">
        <!-- BEGIN BASIC VALIDATION -->
        <div class="row">
            <div class="col-md-12">
                <form class="form form-validate floating-label" enctype="multipart/form-data" novalidate="novalidate" method="post">
                    <div class="card-head style-primary">
                        <div class="tools pull-left">
                            <header>Update  User</header>
                        </div>
                        <div class="tools">
                            <a class="btn btn-flat hidden-xs" href="<?php echo base_url('admin/user'); ?>"><span class="glyphicon glyphicon-arrow-left"></span> &nbsp;Back</a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                             <?php if (@$message): ?>
                                <div class="alert alert-callout alert-success" role="alert">
                                    <strong>Success!</strong> <?php echo $message; ?>
                                </div>
                            <?php endif; ?>
                            <?php if (@$error): ?>
                                <div class="alert alert-callout alert-warning" role="alert">
                                    <strong>Warning!</strong> <?php echo $error; ?>
                                </div>
                            <?php endif; ?>
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" name="name" value="<?php echo $user->name; ?>" required data-rule-minlength="2">
                            </div>
                            <div class="form-group">
                                <label for="alias">Email</label>
                                <input type="text" class="form-control" id="user_email" name="user_email"  value="<?php echo $user->user_email; ?>" required >
                            </div>
                            <div class="form-group">
                                <label for="alias">Country Code</label>
                                <input type="text" class="form-control" id="country_code" name="country_code" readonly="readonly"  value="<?php echo $user->country_code; ?>" >
                            </div>
                            <div class="form-group">
                                <label for="alias">Mobile</label>
                                <input type="text" class="form-control" id="user_mobile" name="user_mobile" readonly="readonly" value="<?php echo $user->user_mobile; ?>" required >
                            </div>
                            <div class="form-group">
                                <label for="alias">Address</label>
                                <input type="text" class="form-control" id="user_address" name="user_address"  value="<?php echo $user->user_address; ?>" >
                            </div>
                            
                            <div class="form-group">
                                <label for="alias">Password</label>
                                <input type="text" class="form-control" id="user_password" name="user_password"   >
                            </div>
                            <div class="form-group">
                                            <div id="img-canvas" class="border-gray height-7" style="    width: 26%;">
                                                <?php if ($user->user_pic != '' && $user->user_pic != null) : ?>
                                                    <img src="<?php echo base_url() . 'uploads/users/profile/' . $user->user_pic; ?>" width="250px">
                                                <?php else: ?>
                                                    <img src="<?php echo base_url(); ?>/assets/img/default-user.jpg" alt="" width="250px"/>
                                                <?php endif; ?>
                                            </div>
                                            <div class="form-group">
                                                <input class="form-control" type="file" name="user_pic" id="user_pic">
                                                <span  class="checkbox checkbox-styled "><label><input name="deleteProfileimage" type="checkbox" id="deleteProfileimage" value="1" > Delete Current Image</label></span>
                                            </div>
                            </div>
                            <div class="form-group">

                                <label for="is_active">Status</label>
                                <select class="form-control" name="is_active" id="is_active" required>
                                    <option value="">Select</option>
                                    <option value="1" <?php echo $user->is_active==1 ? 'selected="selected"':'';?>>Active</option>
                                    <option value="0" <?php echo $user->is_active==0 ? 'selected="selected"':'';?>>Inactive</option>
                                </select>
                            </div>

                        </div><!--end .card-body -->
                        <div class="card-actionbar">
                            <div class="card-actionbar-row">
                                <button type="submit" class="btn ink-reaction btn-raised btn-primary btn-loading-state" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading...">Update<div style="top: 26px; left: 32.5px;" class="ink"></div></button>
                            </div>
                        </div><!--end .card-actionbar -->
                    </div><!--end .card -->
                </form>
            </div><!--end .col -->
        </div><!--end .row -->
        <!-- END BASIC VALIDATION -->

    </div><!--end .section-body -->
</section>