<div class="card style-transparent">
    <div class="card-body">
        <div class="row">
            <div class = "container">
                <div class="doc-registration-form">
                    <div class="login_wrapper wrapper">
                        <form action="" method="post" name="Login_Form" class="form-signin form floating-label">       
                            <h3 class="form-signin-heading">Welcome Back! Please Sign In</h3>
                            <hr class="colorgraph"><br>
                            <?php if (@$error): ?>
                                <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <?php echo $error; ?>
                                </div>
                            <?php endif; ?>
                            <div class="form-group">
                                <input type="text" class="form-control" id="user_email" name="user_email"/>
                                <label for="username">Username</label>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="password" name="password"/>
                                <label for="password">Password</label>
                                <p class="help-block"><a href="<?php echo base_url(); ?>/admin/users/forgot_password">Forgotten?</a></p>
                            </div>			
                            <button class="btn btn-lg btn-primary btn-block"  name="Submit" value="Login" type="Submit">Login</button>  			
                        </form>			
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
