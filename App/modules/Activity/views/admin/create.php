<script src="<?php echo base_url('assets/plugins/ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/ckfinder/ckfinder.js'); ?>"></script>
<section>
    <div class="contain-lg">
        <!-- BEGIN BASIC VALIDATION -->
        <div class="row">
            <div class="col-md-12">
                <form class="form form-validate floating-label" novalidate="novalidate" method="post">
                    <div class="card-head style-primary">
                        <div class="tools pull-left">
                            <header>Add New</header>
                        </div>
                        <div class="tools">
                            <a class="btn btn-flat hidden-xs" href="<?php echo base_url('admin/cms'); ?>"><span class="glyphicon glyphicon-arrow-left"></span> &nbsp;Back</a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <?php if (@$error): ?>
                                <div class="alert alert-callout alert-warning" role="alert">
                                    <strong>Warning!</strong> <?php echo $error; ?>
                                </div>
                            <?php endif; ?>
                            <div class="form-group">
								<label for="title">Title</label>
                                <input type="text" class="form-control" id="body" name="body" required data-rule-minlength="2">
                            </div>
                            <div class="form-group">
                                <label for="alias">Destination</label>
                                <input type="text" class="form-control" id="destination" name="destination" required data-rule-minlength="2">
                            </div>
							<!--
							<div class="form-group">
                                <label for="alias"></label>
                                <input type="text" class="form-control" id="birthday1" name="start_end_date">
                            </div>
						 	 -->
							<div class="form-group" style="display:none;">
								<label for="parent_id">User</label>
                                <input type="hidden" name="user_id" value="1" />
                            </div>
							<div class="form-group">
								<label for="parent_id">Trip</label>
                                <select class="form-control" name="resource_id" id="resource_id" data-table="wo_groups">
                                    <option value="0">Select Trip</option>
                                    <?php foreach ($trips as $trip) { ?>
                                        <option value="<?php echo $trip->trip_id; ?>"><?php echo $trip->title; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
							<div class="form-group">
								<div id="mulitplefileuploader">Upload</div>
								<div id="status"></div>
							</div>
							<div class="form-group">
								<label for="parent_id">Group</label>
                                <select class="form-control" name="group_id[]" id="group_id" multiple>
                                   <?php foreach ($groups as $group) { ?>
                                        <option value="<?php echo $group->group_id; ?>"><?php echo $group->title; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
							<div class="form-group">
								<label for="title">Location URL</label>
                                <input type="text" class="form-control" id="location_url" name="location_url"  data-rule-minlength="2">
                            </div>
							<!--
                            <div class="form-group">
                                <textarea id="editor1" class="form-control control-5-rows" placeholder="Enter text ..." name="description">
                                </textarea>
                            </div>

                            <script>
                                var editor = CKEDITOR.replace('editor1');
                                editor.config.extraAllowedContent = 'div(*)';
                                CKEDITOR.config.allowedContent = true;
                                CKEDITOR.disableAutoInline = true;
                                CKFinder.setupCKEditor(editor, '<?php echo base_url() ?>assets/plugins/ckfinder/');
                            </script>
							-->
                            <div class="form-group">

                                <label for="is_active">Status</label>
                                <select class="form-control" name="is_active" id="is_active" required>
                                    <option value="">Select</option>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>
                        </div><!--end .card-body -->
                        <div class="card-actionbar">
                            <div class="card-actionbar-row">
                                <button type="submit" class="btn ink-reaction btn-raised btn-primary btn-loading-state" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading...">Create<div style="top: 26px; left: 32.5px;" class="ink"></div></button>
                            </div>
                        </div><!--end .card-actionbar -->
                    </div><!--end .card -->
                </form>
            </div><!--end .col -->
        </div><!--end .row -->
        <!-- END BASIC VALIDATION -->

    </div><!--end .section-body -->
</section>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyA_ToK0DnaLaZ3sQmplHhKpRoYa2q5kfKQ"></script>
<script>
// google location
function initialize() {
	var input = document.getElementById('destination');
	var options = {componentRestrictions: {country: 'in'}};
	new google.maps.places.Autocomplete(input, options);
	
	 var input = document.getElementById('searchTextField');
        var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            /* document.getElementById('city2').value = place.name;
            document.getElementById('cityLat').value = place.geometry.location.lat();
            document.getElementById('cityLng').value = place.geometry.location.lng(); */
            console.log(place.geometry.location.lat());

        });
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>
<style>
.ajax-upload-dragdrop{border: 1px solid #ccc;padding-left: 6px;}
.images_files{ width: 32%;
    text-align: center;
    margin-bottom: 10px;
    float: left;
    padding: 0 25px;}    
.images_files .image_location{margin-top: 10px;
    width: 100%;
    float: left;}
div#status {
    width: 100%;
    float: left;
}
.form-group {
    width: 100%;
    float: left;
}
.images_files .img{    width: 100%;
    height: 148px;}
.images_files .img img{    width: 23%;}
.upload-statusbar{display:none;}
</style>