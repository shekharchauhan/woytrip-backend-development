<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions

/**
 * @author          Orange Mantra
 * @license         OM
 */
class Activity extends Admin_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('activity_model');
        $this->load->library("session");
        $this->load->library('pagination');
    }

    function index() {
		//check_auth();
        if ($this->input->post() != null) {
            $this->activity_model->updateStatus();
        }
        $config = array();
        $config["base_url"] = base_url() . "admin/activity/index";
        $config['total_rows'] = $this->activity_model->count_all();
        $config['per_page'] = (site_limit()) ? site_limit() : DEFAULT_LIMIT_PER_PAGE;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["lists"] = $this->activity_model->getList($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['main_content'] = 'activity';
        $data['meta_title'] = "Activity Management";
        $data['meta_keyword'] = "This is an meta keyword";
        $data['meta_description'] = "This is an meta description";
        $includeJs = array(
            'assets/js/libs/jquery/jquery-1.11.2.min.js',
            'assets/js/libs/DataTables/jquery.dataTables.min.js',
            'assets/js/libs/DataTables/extensions/ColVis/js/dataTables.colVis.min.js',
            'assets/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js',
            'assets/js/libs/nanoscroller/jquery.nanoscroller.min.js',
            'assets/js/core/source/App.js',
            'assets/js/core/demo/DemoTableDynamic.js',
        );
        $includeCss = array(
            'assets/css/theme-default/libs/DataTables/jquery.dataTables.css?1423553989',
            'assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css?1423553990',
            'assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css?1423553990',
        );
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $this->setData($data);
    }

    function setting() {
        if ($this->input->post('section'))
            $data['update'] = $this->activity_model->saveBlocks();

        $data['content'] = $this->activity_model->getStaicBlocks();
        $this->setData($data);
    }

    function delete() {
        $id = $this->input->post('delID');
        if (isset($id) and $id != '') {
            $check = $this->activity_model->delete($id);
            if ($check) {
                $data['status'] = true;
                $data['message'] = "Deleted Successfully";
                echo json_encode($data);
            } else {
                $data['status'] = false;
                $data['message'] = "Something Went wrong";
                echo json_encode($data);
            }
        } else {
            $data['status'] = false;
            $data['message'] = "Something Went wrong";
            echo json_encode($data);
        }
    }

    public function create() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        $includeJs = array('assets/js/libs/jquery-validation/dist/jquery.validate.min.js',
            'assets/js/libs/jquery-validation/dist/additional-methods.min.js',
            'assets/js/core/source/App.js',
            'assets/js/activity.js',
        );
        $includeCss = array('theme/plugins/jquery-validation/demo/css/screen.css');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['allParent'] = $this->activity_model->getAllParentPage();
        $data['users'] = $this->activity_model->getListByTable('wo_users');
        $data['trips'] = $this->activity_model->getListByTable('wo_trips');
        $data['groups'] = $this->activity_model->getListByTable('wo_groups');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('body', 'body', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
            $this->setData($data);
        } else {
			
            $this->activity_model->create();
            redirect(base_url('admin/activity'));
        }
    }

    public function getSitetitle() {
        $info = $this->activity_model->getStaicBlocks();
        return $info['contacts']->content;
    }

    public function edit($id) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        $includeJs = array('assets/js/libs/jquery-validation/dist/jquery.validate.min.js',
            'assets/js/libs/jquery-validation/dist/additional-methods.min.js',
            'assets/js/core/source/App.js',
            'theme/js/activity.js',
        );
        $includeCss = array('theme/plugins/jquery-validation/demo/css/screen.css');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['allParent'] = $this->activity_model->getAllParentPage();
		$data['users'] = $this->activity_model->getListByTable('wo_users');
        $data['trips'] = $this->activity_model->getListByTable('wo_trips');
        $data['groups'] = $this->activity_model->getListByTable('wo_groups');
        $data['activity'] = $this->activity_model->get($id);
        $this->load->library('form_validation');
        $this->form_validation->set_rules('body', 'Title', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
            $this->setData($data);
        } else {
            $this->activity_model->update($id);
            redirect(base_url('admin/activity'));
        }
    }
	public function getResultByAjax(){
		$this->activity_model->getResultByAjax($this->input->post('id'),$this->input->post('table'));
	}
	public function uploadImages(){
		$ret = array();
		$output_dir = IMAGESPATH . '/activities/';
		$error =$_FILES["myfile"]["error"];
		if(!is_array($_FILES["myfile"]['name'])) //single file
    	{
            $RandomNum   = time();
            
            $ImageName      = str_replace(' ','-',strtolower($_FILES['myfile']['name']));
            $ImageType      = $_FILES['myfile']['type']; //"image/png", image/jpeg etc.
         
            $ImageExt = substr($ImageName, strrpos($ImageName, '.'));
            $ImageExt       = str_replace('.','',$ImageExt);
            $ImageName      = preg_replace("/\.[^.\s]{3,4}$/", "", $ImageName);
            $NewImageName = $ImageName.'-'.$RandomNum.'.'.$ImageExt;

       	 	move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir. $NewImageName);
       	 	  $ret['file']= $NewImageName;
    	}
    	else
    	{
            $fileCount = count($_FILES["myfile"]['name']);
    		for($i=0; $i < $fileCount; $i++)
    		{
                $RandomNum   = time();
            
                $ImageName      = str_replace(' ','-',strtolower($_FILES['myfile']['name'][$i]));
                $ImageType      = $_FILES['myfile']['type'][$i]; //"image/png", image/jpeg etc.
             
                $ImageExt = substr($ImageName, strrpos($ImageName, '.'));
                $ImageExt       = str_replace('.','',$ImageExt);
                $ImageName      = preg_replace("/\.[^.\s]{3,4}$/", "", $ImageName);
                $NewImageName = $ImageName.'-'.$RandomNum.'.'.$ImageExt;
                
                $ret[$NewImageName]= $output_dir.$NewImageName;
    		    move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$output_dir.$NewImageName );
    		}
    	}
		echo json_encode($ret);
	}
}
