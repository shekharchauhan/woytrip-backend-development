<?php
/**
 * Created by PhpStorm.
 * User: Parmod Bhardwaj<parmod@orangemantra.com>
 * Date: 10/13/2015
 * Time: 11:02 AM
 */

class Country_model extends CI_Model{
    var $country_table = "country";
    var $state_table = "state";
    function __construc() {
        parent::__construct();
    }

    /**
     * @param null $country_code
     * @return dara array
     * @author Parmod Bhardwaj<parmod@orangemantra.com>
     * @date : 13/10/15
     * @description : get Country and State data
     */
    public function getData( $country_code = null) {
        //echo $country_code;die;
        if ($country_code != null) {
            if($country_code!='ALL'){
             $this->db->where('country_code', $country_code);   
            }
            //$this->db->where('state_status', '1');
            $this->db->from($this->state_table);
        }
        else{
            //$this->db->where('country_status','1');
            $this->db->from($this->country_table);
        }
        $result = $this->db->get();
        return $result->result();
    }

} 