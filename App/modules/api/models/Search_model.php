<?php

/**
 * Created by PhpStorm.
 * User: Gaurav Ranjan <ranjan.gaurav@orangemantra.in>
 * Date: 05/10/2016
 * Time: 11:28 AM
 */
class Search_model extends CI_Model {
	var $user_table = "wo_users";
	var $device_table = "wo_user_devices";
	var $friends_table = "wo_friends";
	// var $user_table = "wo_users";
	var $members_table = "wo_group_members";
	function __construct() {
		parent::__construct ();
	}
	function str_replace_first($from, $to, $subject) {
		$from = '/' . preg_quote ( $from, '/' ) . '/';
		
		return preg_replace ( $from, $to, $subject, 1 );
	}
	public function getUsersData($text, $userid, $friends) {
		if ($userid) {
			$this->db->where_in ( 'user_id', $friends );
			$this->db->like ( 'name', $text);
		} 

		else {
			$this->db->like ( 'name', $text);
		}
		
		$this->db->from ( 'wo_users' );
		$this->db->select ( 'wo_users.*,if(wo_users.user_pic="","null" ,CONCAT("' . base_url ( 'uploads/users/profile' ) . '/",wo_users.user_pic ) ) user_pic' );
		$result = $this->db->get ();  //echo $this->db->last_query(); die();
		return $result->result_array ();
	}
	public function getAlltrips($text, $userid, $friends) {
		if ($userid) {
			
			$friends = implode ( ',', $friends );
			// $friends = explode(',',$friends);
			
			// print_r($friends); die();
			
			$query = $this->db->query ( "SELECT `wo_trips`.*, `wo_users`.`name`
                            FROM `wo_trips`
                            JOIN `wo_users` ON `wo_users`.`user_id` = `wo_trips`.`user_id`
                            WHERE `wo_trips`.`user_id` = '$userid'
                            AND  CASE WHEN (title != '') THEN `title` LIKE CONCAT('%','$text','%') ESCAPE '!' ELSE `destination` LIKE CONCAT('%','$text','%') ESCAPE '!' END
                            OR `wo_trips`.`user_id` IN($friends)
                            AND  CASE WHEN (title != '') THEN `title` LIKE CONCAT('%','$text','%') ESCAPE '!' ELSE `destination` LIKE CONCAT('%','$text','%') ESCAPE '!' END" );
			//print_r($query); die();
			// $this->db->where("NOT EXISTS (SELECT title FROM wo_trips WHERE title like '.$text.'%')");
		} 

		else {
			// $this->db->like ( 'title', $text, 'after' );
			
			$query = $this->db->query ( "SELECT `wo_trips`.*, `wo_users`.`name`
					FROM `wo_trips`
					JOIN `wo_users` ON `wo_users`.`user_id` = `wo_trips`.`user_id`
					CASE WHEN (title != '') THEN `title` LIKE CONCAT('%','$text','%') ESCAPE '!' ELSE `destination` LIKE CONCAT('%','$text','%') ESCAPE '!' END" );
		}
		$result = $query->result_array ();
		// $this->db->select('wo_trips.*,wo_users.name');
		// $this->db->join('wo_users','wo_users.user_id = wo_trips.user_id');
		// $this->db->from ( 'wo_trips' );
		// $result = $this->db->get ();
		// echo $this->db->last_query(); die();
		$array = array ();
		foreach ( $result as $row ) {
			if ($row ['title'] != "") {
				$row ['trip_name'] = $row ['title'];
			} 

			else {
				$row ['trip_name'] = $row ['destination'];
			}
			array_push ( $array, $row );
		}
		
		// print_r($array); die();
		
		return $array;
	}
	public function getAllgroups($text, $userid, $group) {
		if ($userid) {
			$this->db->where ( 'wo_groups.user_id', $userid );
			$this->db->like ( 'title', $text);
			if($group)
			{
			$this->db->or_where_in ( 'wo_groups.user_id', $group );
			}
			$this->db->like ( 'title', $text);
		} 

		else {
			$this->db->like ( 'title', $text);
		}
		$this->db->from ( 'wo_groups' );
		$result = $this->db->get (); //echo $this->db->last_query(); die();
		$output = array ();
		foreach ( $result->result_array () as $group ) {
			// $output = $group;
			$group ['members'] = $this->getGroupMembers ( $group ['group_id'] );
			
			array_push ( $output, $group );
			// $i ++;
		}
		return $output;
	}
	public function GetAllFriends($userid) {
		$query = $this->db->query ( '
        SELECT wo_friends.friend_id
        FROM `wo_friends`
        WHERE `user_id` = "' . $userid . '"
        UNION
        SELECT wo_friends.user_id as  friend_id
        FROM `wo_friends`
        WHERE `friend_id` = "' . $userid . '"' );
		// echo $this->db->last_query(); die();
		$result = $query->result_array (); // print_r($result); die();
		return $result;
	}
	public function GetAllgroupID($userid) {
		$this->db->where ( 'user_id', $userid );
		$this->db->select ( 'group_id' );
		$query = $this->db->get ( 'wo_group_members' );
		$result = $query->result_array (); // print_r($result); die();
		return $result;
	}
	public function getGroupMembers($group_id) {
		$this->db->distinct ();
		$this->db->select ( 'if(us.user_pic="","null" ,CONCAT("' . base_url ( 'uploads/users/profile' ) . '/",us.user_pic ) ) user_pic,us.name,us.user_id,us.user_mobile' );
		$this->db->where ( 'mem.group_id', $group_id );
		$this->db->where ( 'us.is_active', '1' );
		$this->db->from ( $this->members_table . ' mem' );
		$this->db->join ( $this->user_table . ' us', 'us.user_id=mem.user_id' ); // echo $this->db->last_query(); //die();
		$members = $this->db->get ();
		return $members->result ();
	}
}

?>