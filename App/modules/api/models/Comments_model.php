<?php

/*
 * Author : Gaurav Ranjan
 *
 * Date : 09/01/2017
 *
 */
class Comments_model extends CI_Model {
	var $comment_table = "wo_activity_comments";
	var $user_table = "wo_users";
	var $activity_table = "wo_activities";
	function __construct() {
		parent::__construct ();
		date_default_timezone_set ( 'Asia/Kolkata' );
	}
	public function insertComments($data) {
		try {
			$this->load->library ( 'form_validation' );
			$config = array (
					array (
							'field' => 'poster_id',
							'label' => 'UserID',
							'rules' => 'trim|required' 
					),
					array (
							'field' => 'resource_id',
							'label' => 'Resource ID',
							'rules' => 'trim|required' 
					),
					array (
							'field' => 'resource_type',
							'label' => 'Type',
							'Please Define type' => 'trim|required' 
					),
					array (
							'field' => 'body',
							'label' => 'Comment',
							'Please enter comment' => 'trim|required' 
					) 
			);
			
			$this->form_validation->set_rules ( $config );
			if ($this->form_validation->run () == false) {
				$errors_array = '';
				foreach ( $config as $row ) {
					$field = $row ['field'];
					$error = strip_tags ( form_error ( $field ) );
					if ($error)
						$errors_array .= $error . ', ';
				}
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => rtrim ( $errors_array, ', ' ) 
				);
			} else {
				$data_comments = array (
						'poster_id' => $data ['poster_id'],
						'resource_id' => $data ['resource_id'],
						'resource_type' => $data ['resource_type'],
						'body' => $data ['body'],
						'creation_date' => date ( 'Y-m-d H:i:s' ) 
				);
				$this->db->insert ( $this->comment_table, $data_comments );
				$insertId = $this->db->insert_id ();
				if ($insertId) {
					$datas ['modification_date'] = date ( 'Y-m-d H:i:s' );
					$this->db->where ( 'activity_id', $data ['resource_id'] );
					$this->db->update ( $this->activity_table, $datas ); // echo $this->db->last_query(); die();
				}
				// $this->db->where
				//
				
				$message = array (
						'status' => true,
						'response_code' => '1',
						'comment_id' => $insertId 
				);
			}
		} catch ( Exception $ex ) {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
		}
		
		return $message;
	}
	public function getCountComment($id) {
		$this->db->select ( 'comment_count,user_id' );
		$this->db->from ( $this->activity_table );
		$this->db->where ( 'activity_id', $id );
		$res = $this->db->get ();
		return $res->row ();
	}
	public function updateCommentCounter($value, $activity_id) {
		$data = array (
				'comment_count' => $value 
		);
		$this->db->where ( 'activity_id', $activity_id );
		$this->db->update ( $this->activity_table, $data );
	}
	public function getAllComments($resource_id) {
		$this->db->select ( 'comm.comment_id,comm.resource_id,comm.body,us.user_id,if(us.user_pic="","null" ,CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",us.user_pic ) ) user_pic,us.name,comm.creation_date' );
		$this->db->where ( 'resource_id', $resource_id );
		$this->db->order_by ( 'creation_date', 'ASC' );
		$this->db->from ( $this->comment_table . ' comm' );
		$this->db->join ( $this->user_table . ' us', '`us`.`user_id`=`comm`.`poster_id`' );
		$res = $this->db->get ();
		return $res->result_array ();
	}
	public function delete($id) {
		if (isset ( $id ) && $id != '') {
			$this->db->where ( 'comment_id', $id );
			$query = $this->db->delete ( $this->comment_table );
			if ($query) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	public function getActivity($id) {
		$this->db->select ( 'resource_id' );
		$this->db->where ( 'comment_id', $id );
		$this->db->from ( $this->comment_table );
		$res = $this->db->get ();
		if ($res->num_rows () > 0) {
			return $res->row ()->resource_id;
		} else {
			return false;
		}
	}
	public function updateComment($id) {
		$comment_count = $this->db->select ( 'comment_count' )->where ( 'activity_id', $id )->get ( $this->activity_table )->row ()->comment_count;
		$data = array (
				'comment_count' => $comment_count - 1 
		);
		$this->db->where ( 'activity_id', $id );
		$this->db->update ( $this->activity_table, $data );
	}
}
