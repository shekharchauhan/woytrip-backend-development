<?php
require APPPATH . '/libraries/aws.phar';
class Group_model extends CI_Model {
	var $table = "wo_groups";
	var $user_table = "wo_users";
	var $members_table = "wo_group_members";
	var $trip = "wo_trips";
	function __construct() {
		parent::__construct ();
		date_default_timezone_set ( 'Asia/Kolkata' );
		error_reporting ( 0 );
	}
	
	/*
	 * @Param: user_id, title, media_src, members(this is json object of user_ids)
	 * @Add New Group
	 * @Table: wo_groups
	 */
	public function createGroup() {
		
		// echo 333; die();
		try {
			$this->load->library ( 'form_validation' );
			$config = array (
					array (
							'field' => 'user_id',
							'label' => 'UserID',
							'rules' => 'trim|required' 
					) 
			);
			
			$this->form_validation->set_rules ( $config );
			if ($this->form_validation->run () == false) {
				$errors_array = '';
				foreach ( $config as $row ) {
					$field = $row ['field'];
					$error = strip_tags ( form_error ( $field ) );
					if ($error)
						$errors_array .= $error . ', ';
				}
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => rtrim ( $errors_array, ', ' ) 
				);
			} else {
				
				$data = array (
						'user_id' => $this->input->post ( 'user_id' ),
						'title' => ($this->input->post ( 'title' )) ? $this->input->post ( 'title' ) : $this->input->post ( 'destination' ),
						'is_active' => '1',
						'member_count' => '0',
						'creation_date' => date ( 'Y-m-d H:i:s' ),
						'modification_date' => date ( 'Y-m-d H:i:s' ) 
				);
				$file = $this->uploadGroupPic ();
				if ($file ['file_name'] != '') {
					$data ['media_src'] = $file ['file_name'];
				}
				$insert = $this->db->insert ( $this->table, $data );
				if ($insert) {
					$insertId = $this->db->insert_id ();
					$add = $this->addGroupMembers ( $insertId );
					// $this->SendInvitation ( $add );
					$message = array (
							'status' => true,
							'response_code' => '1',
							'group_id' => $insertId,
							'message' => 'Success' 
					);
				} else {
					$message = array (
							'status' => false,
							'response_code' => '0',
							'message' => 'Database Error Occured' 
					);
				}
			}
		} catch ( Exception $ex ) {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
		}
		
		return $message;
	}
	
	/*
	 * @Param: group_id
	 * @Add Members to a group
	 * @Table: wo_group_members
	 *
	 */
	public function addGroupMembers($groupId) {
		error_reporting ( 0 );
		$NonMembers = $this->input->post ( 'non_members' ); // print_r($NonMembers); die();
		
		if ($NonMembers) {
			$NonMembers = json_decode ( $this->input->post ( 'non_members' ) );
			$checkNewUsers = $this->GetNewusers ( $NonMembers );
			if (count ( $checkNewUsers ) == 0) {
				$AddNewUsers = $this->AddNewusers ( $NonMembers ); // print_r($AddNewUsers); //die();
			} 

			else {
				
				$AddNewUsers = array_column ( $checkNewUsers, 'user_id' );
			}
			
			$members = json_decode ( $this->input->post ( 'members' ) );
			if ($members) {
				array_push ( $members, $this->input->post ( 'user_id' ) );
			}
			
			$members = array_merge ( $members, $AddNewUsers ); // print_r($members); die();
		} 

		else {
			$members = json_decode ( $this->input->post ( 'members' ) );
			if ($members) {
				array_push ( $members, $this->input->post ( 'user_id' ) );
			}
		}
		if (! empty ( $members )) {
			$IDs = array ();
			$i = 0;
			foreach ( $members as $memberId ) {
				$IDs [$i] ['group_id'] = $groupId;
				$IDs [$i] ['user_id'] = $memberId;
				
				$i ++;
			}
			
			$this->deleteGroupMembers ( $groupId );
			$this->db->insert_batch ( $this->members_table, $IDs );
			return $NonMembers;
		} else {
			return false;
		}
	}
	public function GetNewusers($NonMembers) {
		$this->db->where_in ( 'user_mobile', $NonMembers );
		$this->db->select ( 'user_id' );
		$result = $this->db->get ( 'wo_users' ); // echo $this->db->last_query(); die();
		return $result->result_array ();
	}
	
	/*
	 * First Delete All the Members of Group based on the group_id
	 * @Table: wo_group_members
	 * @param: group_id
	 */
	public function deleteGroupMembers($groupID) {
		$this->db->where ( 'group_id', $groupID );
		$this->db->delete ( $this->members_table );
	}
	
	/*
	 * @Param: group_id
	 * @To Update an group informatioon
	 * @Also the members of group
	 * @Table: wo_groups
	 */
	public function update($groupId) {
		error_reporting ( 0 );
		try {
			$postData = $this->input->post ();
			
			unset ( $postData ['key'] );
			unset ( $postData ['members'] );
			unset ( $postData ['group_media_src'] );
			/*
			 * $file = $this->uploadGroupPic ();
			 *
			 * if ($file ['file_name'] != '') {
			 * $postData ['media_src'] = $file ['file_name'];
			 * }
			 */
			
			$data = array (
					'title' => $postData ['title'] 
			);
			
			$file = $this->uploadGroupPic ();
			if ($file ['file_name'] != '') {
				$data ['media_src'] = $file ['file_name'];
			}
			
			// print_r($postData);die();
			
			$this->db->set ( 'modification_date', 'NOW()', false );
			$this->db->where ( 'group_id', $groupId );
			$update = $this->db->update ( $this->table, $data );
			if ($update) {
				$this->addGroupMembers ( $groupId );
				$message = array (
						'status' => true,
						'response_code' => '1',
						'message' => "Group Updated Successfully" 
				);
			} else {
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => "Database error occured" 
				);
			}
		} catch ( Exception $ex ) {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
		}
		return $message;
	}
	
	/*
	 * @Param: user_id
	 * @To get the List of groups of a user
	 * @Also the members of group
	 * @Table: wo_groups , wo_group_members
	 */
	public function getAllUserGroups($user_id) {
		$this->db->select ( 'gr.group_id,gr.user_id,gr.title as group_name,if(us.user_pic="","null" ,CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",us.user_pic ) ) user_pic,if(gr.media_src="","null" ,CONCAT("' . base_url ( 'uploads/groups/' ) . '/",gr.media_src ) ) media_src,us.name as user_name,gr.creation_date' );
		$this->db->where ( 'us.user_id', $user_id );
		$this->db->order_by ( 'tr.creation_date', 'DESC' );
		$this->db->group_by ( 'gr.group_id' );
		$this->db->from ( $this->user_table . ' us' );
		$this->db->join ( $this->members_table . ' mem', '`us`.`user_id`=`mem`.`user_id`' );
		$this->db->join ( $this->table . ' gr', '`mem`.`group_id`=`gr`.`group_id`' );
		$this->db->join ( $this->trip . ' tr', '`tr`.`group_id`=`gr`.`group_id` and tr.trip_status = "ongoing"', 'left' );
		$res = $this->db->get ();  echo $this->db->last_query(); die();
		$groups = $res->result ();
		$output = array ();
		$i = 0;
		foreach ( $groups as $group ) {
			$output [$i] = $group;
			$output [$i]->members = $this->getGroupMembers ( $group->group_id );
			$i ++;
		}
		return $output;
	}
	
	/**
	 * ription : get the Members of a group
	 *
	 * @param
	 *        	group_id
	 * @return bool @date : 19/10/2016
	 * @method : Get the Members of a group
	 */
	public function getGroupMembers($group_id) {
		$this->db->distinct ();
		$this->db->select ( 'if(us.user_pic="","null" ,CONCAT("' . base_url ( 'uploads/users/profile' ) . '/",us.user_pic ) ) user_pic,us.name,us.user_id,us.user_mobile' );
		$this->db->where ( 'mem.group_id', $group_id );
		$this->db->where ( 'us.is_active', '1' );
		$this->db->from ( $this->members_table . ' mem' );
		$this->db->join ( $this->user_table . ' us', 'us.user_id=mem.user_id' ); // echo $this->db->last_query(); //die();
		$members = $this->db->get ();
		return $members->result ();
	}
	
	/**
	 * ription : To Upload Group Pic
	 *
	 * @param
	 *        	$id
	 * @return bool @date : 05/10/2016
	 * @method : Upload Group Pic
	 */
	protected function uploadGroupPic() {
		$config = array ();
		ini_set ( 'upload_max_filesize', '200M' );
		ini_set ( 'post_max_size', '200M' );
		ini_set ( 'max_input_time', 3000 );
		ini_set ( 'max_execution_time', 3000 );
		$config ['upload_path'] = IMAGESPATH . 'groups/';
		$config ['allowed_types'] = '*';
		$config ['file_name'] = md5 ( uniqid ( rand (), true ) );
		$this->load->library ( 'upload', $config );
		$this->upload->initialize ( $config );
		if ($this->upload->do_upload ( 'group_media_src' )) {
			$info = $this->upload->data ();
			return $info;
		}
	}
	
	/**
	 * ription : To delete an group
	 *
	 * @param
	 *        	group_id
	 * @return bool @date : 19/10/2016
	 * @method : Delete an group
	 */
	public function delete($id) {
		if (isset ( $id ) && $id != '') {
			$this->db->where ( 'group_id', $id );
			$query = $this->db->delete ( $this->table );
			if ($query) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	public function GetGroupAdmin($id) {
		$this->db->where ( 'wo_groups.group_id', $id );
		$this->db->select ( 'wo_groups.user_id,wo_groups.title' );
		$this->db->from ( $this->table );
		$res = $this->db->get ();
		// echo $this->db->last_query(); die();
		return $res->row ();
	}
	public function RemoveGroupMember($groupid, $userid) {
		$this->db->where ( 'user_id', $userid );
		$this->db->where ( 'group_id', $groupid );
		$query = $this->db->delete ( $this->members_table );
		
		// echo $this->db->last_query(); die();
		if ($query) {
			return true;
		} else {
			return false;
		}
	}
	public function SeeGroupMembers($groupid) {
		
		// echo 123;
		// exit;
		$this->db->where ( 'wo_groups.group_id', $groupid );
		$this->db->select ( 'wo_users.*' );
		$this->db->from ( $this->table );
		$this->db->join ( $this->members_table, 'wo_groups.group_id = wo_group_members.group_id' );
		$this->db->join ( $this->user_table, 'wo_group_members.user_id = wo_users.user_id' );
		$res = $this->db->get ();
		// echo $this->db->last_query(); die();
		return $res->result ();
	}
	public function updateGroupAdmin($groupid, $value) {
		$this->db->set ( 'modification_date', 'NOW()', FALSE );
		$this->db->where ( 'group_id', $groupid );
		$update = $this->db->update ( $this->table, $value );
		
		if ($update) {
			$this->db->where ( 'wo_groups.group_id', $groupid );
			$this->db->select ( 'wo_groups.*' );
			$this->db->from ( $this->table );
			$res = $this->db->get ();
			return $res->row ();
		} 

		else {
			return false;
		}
	}
	public function CountGroupMember($groupid) {
		$this->db->where ( 'wo_groups.group_id', $groupid );
		$this->db->select ( 'wo_groups.*' );
		$this->db->from ( $this->table );
		$this->db->join ( $this->members_table, 'wo_groups.group_id = wo_group_members.group_id' );
		$res = $this->db->get ();
		// echo $this->db->last_query(); die();
		return $res->num_rows ();
	}
	public function deviceType($arr) {
		$this->db->where_in ( 'user_id', $arr );
		$res = $this->db->get ( 'wo_user_devices' );
		
		// echo $this->db->last_query(); die();
		return $res->result_array ();
	}
	public function AddNewusers($NonMembers) {
		$userID = array ();
		$i = 0;
		foreach ( $NonMembers as $num ) {
			$data ['user_mobile'] = $num;
			$data ['is_active'] = '0';
			$data ['country_code'] = '+91';
			$this->db->set ( 'creation_date', 'NOW()', false );
			$this->db->set ( 'modification_date', 'NOW()', false );
			$this->db->insert ( $this->user_table, $data );
			$userID [$i] = $this->db->insert_id ();
			$i ++;
		}
		return $userID;
		// print_r($userID); die();
	}
	public function SendInvitation($NonMembers) {
		// echo 123000; print
		// print_r($NonMembers);
		$sns = Aws\Sns\SnsClient::factory ( array (
				'version' => '2010-03-31',
				'region' => 'ap-southeast-1',
				'credentials' => array (
						'key' => 'AKIAJAC3UZE2HG244DNQ',
						'secret' => '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' 
				) 
		) );
		$result = $sns->listSubscriptionsByTopic ( array (
				'TopicArn' => 'arn:aws:sns:ap-southeast-1:581428970236:woytrip-stage-reg' 
		) );
		foreach ( $result as $key => $value ) {
			if ($key == 'Subscriptions' && count ( $value )) {
				foreach ( $value as $k => $v ) {
					$result = $sns->unsubscribe ( array (
							// SubscriptionArn is required
							'SubscriptionArn' => $v ['SubscriptionArn'] 
					) );
				}
			}
		}
		
		foreach ( $NonMembers as $mem ) {
			$sns->subscribe ( array (
					// TopicArn is required
					'TopicArn' => 'arn:aws:sns:ap-southeast-1:581428970236:woytrip-stage-reg',
					// Protocol is required
					'Protocol' => 'sms',
					'Endpoint' => '+91' . $mem 
			) );
			$res = $sns->publish ( array (
					'TopicArn' => 'arn:aws:sns:ap-southeast-1:581428970236:woytrip-stage-reg',
					'Message' => "You are invited to join WOY TRIP app." 
			) );
		}
		
		// return true;
	}
}
