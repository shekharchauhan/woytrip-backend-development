<?php

/**
 * Created by PhpStorm.
 * User: Gaurav Ranjan<ranjan.gaurav@orangemantra.com>
 * Date: 19/10/2016
 * Time: 11:28 AM
 */
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
class Groups extends REST_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->model ( 'Group_model', 'group' );
		$this->load->model ( 'activity_model', 'activity' );
		$this->load->model ( 'user_model', 'user' );
		date_default_timezone_set ( 'Asia/Kolkata' );
	}
	
	/*
	 * To create a new group
	 * @Params: user_id, title, media_src, members(this is json object of user_ids)
	 * @Table: wo_groups
	 */
	public function create_post() {
		$userInfo = $this->user->getUserInfoById ( $this->input->post ( 'user_id' ) );
		$create = $this->group->createGroup ();
		$notification = array (
				'title' => $userInfo->name . ' has added you in new woy group',
				'message' => $this->input->post ( 'title' ),
				'subject_id' => $create ['group_id'],
				'subject_type' => 'new_group',
				'user_id' => $this->input->post ( 'user_id' ),
				'user_name' => $userInfo->name,
				'user_pic' => $userInfo->user_pic 
		);
		$this->sendNotification ( '', $notification, true );
		$this->set_response ( $create, REST_Controller::HTTP_OK );
	}
	
	/*
	 * To get the list of Groups of an particular User
	 * @Params: user_id
	 * @Table: wo_groups
	 */
	public function list_post() {
		if (! $this->post ( 'user_id' ) && $this->post ( 'user_id' ) == '' && $this->post ( 'user_id' ) == null) {
			$this->set_response ( [ 
					'status' => false,
					'response_code' => '0',
					'message' => 'Please Send User Id' 
			], REST_Controller::HTTP_OK );
		}
		$user_id = $this->post ( 'user_id' );
		$groups = $this->group->getAllUserGroups ( $user_id );
		if (! empty ( $groups )) {
			$this->set_response ( [ 
					'status' => true,
					'response_code' => '1',
					'data' => $groups 
			], REST_Controller::HTTP_OK );
		} else {
			$this->set_response ( [ 
					'status' => false,
					'response_code' => '0',
					'message' => 'No group Available' 
			], REST_Controller::HTTP_OK ); // NOT_FOUND (404) being the HTTP response code
		}
	}
	
	/**
	 *
	 * @method : Update an Group
	 * @method description: Updating a Group
	 * @param
	 *        	: group_id
	 *        	@data: UPdate user to wo_groups table
	 */
	public function update_post() {
		error_reporting ( 0 );
		try {
			foreach ( (($this->post ())) as $key => $value ) {
				log_message ( 'info', 'data=' . $key . ' =>' . $value );
			}
			$groupId = $this->post ( 'group_id' );
			if ($groupId == '') {
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => 'Please send Group id' 
				);
				$this->set_response ( $message, REST_Controller::HTTP_OK );
			} else {
				$update = $this->group->update ( $groupId );
				$this->set_response ( $update, REST_Controller::HTTP_OK );
			}
		} catch ( Exception $ex ) {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		}
	}
	
	/*
	 * To delete an Group
	 * @Params: group_id
	 * @Table: wo_groups
	 */
	public function drop_post() {
		$id = ( int ) $this->post ( 'group_id' );
		if ($id == 0) {
			$message = [ 
					'response_code' => '0',
					'message' => 'Please send group Id' 
			];
			$this->response ( $message, REST_Controller::HTTP_OK ); // BAD_REQUEST (400) being the HTTP response code
		}
		$result = $this->group->delete ( $id );
		if ($result) {
			$message = [ 
					'response_code' => '1',
					'message' => 'Group Deleted' 
			];
			
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		} else {
			$message = [ 
					'response_code' => '0',
					'message' => 'Bad request' 
			];
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		}
	}
	
	/*
	 * Author : Gaurav Ranjan
	 * Date : 04-11-2016
	 * Description : To remove the member of the group
	 *
	 *
	 */
	public function remove_group_member_post() {
		$groupid = $this->input->post ( 'group_id' );
		$userid = $this->input->post ( 'user_id' );
		$userInfo = $this->user->getUserInfoById ( $userid );
		if ($groupid != '') {
			
			$admin = $this->group->GetGroupAdmin ( $groupid );
			$count = $this->group->CountGroupMember ( $groupid );
			
			if ($count > 1) 

			{
				
				$delete = $this->group->RemoveGroupMember ( $groupid, $userid );
				
				if ($delete) {
					
					$notification = array (
							'message' => $userInfo->name . ' has left from ' . $admin->title . ' group',
							'title' => 'Group Notification',
							'subject_id' => '',
							'subject_type' => 'remove_group',
							'user_id' => $userid,
							'user_name' => $userInfo->name,
							'user_pic' => $userInfo->user_pic 
					);
					if ($userid != $admin->user_id) {
						$this->RemoveGroupSendNotification ( $admin->user_id, $notification );
					}
					
					$message = [ 
							'status' => true,
							'response_code' => '1',
							'message' => 'Member Deleted' 
					];
					
					if ($userid == $admin->user_id) {
						$Getusers = $this->group->SeeGroupMembers ( $groupid );
						
						$listofusers = '';
						foreach ( $Getusers as $list ) {
							
							$listofusers [] = $list->user_id;
						}
						
						$key = array_rand ( $listofusers );
						$value ['user_id'] = $listofusers [$key];
						
						$update = $this->group->updateGroupAdmin ( $groupid, $value );
						
						if ($update) {
							$notification = array (
									'message' => 'You are the admin of ' . $update->title . ' ' . 'group',
									'title' => 'Group Notification',
									'subject_id' => '',
									'subject_type' => 'remove_group',
									'user_id' => $userid,
									'user_name' => $userInfo->name,
									'user_pic' => $userInfo->user_pic 
							);
							
							$this->RemoveGroupSendNotification ( $update->user_id, $notification );
						}
					}
				}
			} 

			else {
				
				$result = $this->group->delete ( $groupid );
				if ($result) {
					$message = [ 
							'status' => true,
							'response_code' => '1',
							'message' => 'Member Deleted' 
					];
				}
			}
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		}
	}
	
	/*
	 * Author : Gaurav Ranjan
	 * Date : 12-11-2016
	 * Description : To send Push Notification
	 *
	 *
	 */
	public function sendNotification($userId = '', $data = array(), $send_friends = false) {
		// echo 123; die();
		if ($send_friends) {
			$friends = $this->input->post ( 'members' );
			$friends = json_decode ( $friends ); // print_r($friends); die();
			$androidDeviceIds = getUserDeviceIDs2 ( $friends, 'friend_id' );
		} else {
			
			$ids = ( array ) $userId;
			$androidDeviceIds = getUserDeviceIDs ( $ids );
		}
		$notificationData = $data;
		if (! empty ( $androidDeviceIds )) :
			androidNotification ( $androidDeviceIds, $notificationData );
		
		
		
		endif;
	}
	
	/*
	 * Author : Gaurav Ranjan
	 * Date : 12-11-2016
	 * Description : To send Push Notification
	 *
	 *
	 */
	public function RemoveGroupSendNotification($userId, $data) {
		if ($userId) {
			
			$androidDeviceIds = getUserDeviceIDs2 ( $userId ); // print_r($androidDeviceIds); die();
		}
		$notificationData = $data;
		if (! empty ( $androidDeviceIds )) :
			androidNotification ( $androidDeviceIds, $notificationData );
		
		
		
		endif;
	}
}