<?php

/**
 * Created by PhpStorm.
 * User: Sanjay Yadav<yadav.sanjay@orangemantra.com>
 * Date: 10/10/2016
 * Time: 11:28 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Comments extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Comments_model', 'comment');
        $this->load->model('activity_model', 'activity');
        $this->load->model('user_model', 'user');
        date_default_timezone_set('Asia/Kolkata');
    }

    /*
     * To Add An new Comment to Activity
     * @Params: resource_id,resource_type,poster_id,body
     * @Table: wo_activity_comments
     */

    public function create_post() {
        $resource_id = (int) $this->post('resource_id');
        $data = $this->post();
        $comments = $this->comment->insertComments($data);
        if ($comments['status']) {
            $comment_counter = $this->comment->getCountComment($resource_id);
            $increment_counter = $comment_counter->comment_count + 1;
            $this->comment->updateCommentCounter($increment_counter, $resource_id);
            $userInfo = $this->user->getUserInfoById($this->input->post('poster_id')); //print_r($comment_counter->user_id); die();
            $notification = array(
            		'title' => $userInfo->name .' has commented on  your post',
            		'message' => $this->input->post('body'),
            		'subject_id' => $this->post('resource_id'),
            		'subject_type' => 'Comments',
            		'user_id' => $this->input->post('poster_id'),
            		'user_name' => $userInfo->name,
            		'user_pic' => $userInfo->user_pic,
            );
            //$this->sendNotification($comment_counter->user_id , $notification, false);
            $this->set_response([
                'status' => true,
                'response_code' => '1',
                'comments' => 'success',
                'comment_id' => $comments['comment_id'],
                'data' => 'success'
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->set_response($comments, REST_Controller::HTTP_OK);
        }
    }

    /*
     * To get the list of comments of an particular Activity
     * @Params: resource_id
     * @Table: wo_activity_comments
     */

    public function list_post() {
        if (!$this->post('resource_id') && $this->post('resource_id') == '' && $this->post('resource_id') == null) {
            $this->set_response([
                'status' => false,
                'response_code' => '0',
                'message' => 'No Content'
                    ], REST_Controller::HTTP_OK);
        }
        $resource_id = $this->post('resource_id');
        $comments = $this->comment->getAllComments($resource_id);
        if (!empty($comments)) {
            $this->set_response([
                'status' => true,
                'response_code' => '1',
                'data' => $comments
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->set_response([
                'status' => false,
                'response_code' => '0',
                'message' => 'No Content'
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    /*
     * To delete an Comment 
     * @Params: comment_id
     * @Table: wo_activity_comments
     */

    public function drop_post() {
        $id = (int) $this->post('comment_id');
        if ($id == 0) {
            $message = [
                'response_code' => '0',
                'message' => 'No Comment ID'
            ];
            $this->response($message, REST_Controller::HTTP_OK);    // BAD_REQUEST (400) being the HTTP response code
        }
        $resource_id = $this->comment->getActivity($id);
        if ($resource_id) {
            $this->comment->updateComment($resource_id);
            $result = $this->comment->delete($id);
        } else {
            $result = false;
        }
        if ($result) {
            $message = [
                'response_code' => '1',
                'message' => 'Comment Deleted'
            ];

            $this->set_response($message, REST_Controller::HTTP_OK);
        } else {
            $message = [
                'response_code' => '0',
                'message' => 'Bad request'
            ];
            $this->set_response($message, REST_Controller::HTTP_OK);
        }
    }
    
    public function sendNotification($userId = '', $data = array(), $send_friends = false) {
    	if ($send_friends) {
    		$friends = $this->activity->getUserFriendsList($data['user_id']);
    		$androidDeviceIds = getUserDeviceIDs(array_column($friends, 'friend_id'));
    	} else {
    		$ids = (array) $userId;
    		$androidDeviceIds = getUserDeviceIDs($ids);
    	}
    	$notificationData = $data;
    	if (!empty($androidDeviceIds)):
    	androidNotification($androidDeviceIds, $notificationData);
    	endif;
    }
    

}
