<?php

/**
 * Created by PhpStorm.
 * User: Gaurav Ranjan<ranjan.gaurav@orangemantra.in>
 * Date: 27/10/2016
 * Time: 11:28 AM
 */
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
class Notification extends REST_Controller {
	function __construct() {
		// Construct the parent class
		parent::__construct ();
		$this->load->model ( 'notification_model', 'notification' );
		$this->load->helper ( 'string' );
	}
	
	/*
	 * @ To get All the Notifications of a User
	 * @ params: user_id
	 * @ Table: wo_notifications
	 */
	public function index_post() {
		try {
			$user_id = $this->post ( 'user_id' );
			if ($user_id == '') {
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => 'Please send user_id' 
				);
			} else {
				$result = $this->notification->getNotifications ( $user_id );
				if (! empty ( $result )) {
					$message = array (
							'status' => true,
							'response_code' => '1',
							'message' => 'Success',
							'data' => $result 
					);
				} else {
					$message = array (
							'status' => false,
							'response_code' => '0',
							'message' => 'No Notification Available' 
					);
				}
			}
		} catch ( Exception $ex ) {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
		}
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
	
	/*
	 * @ To Change the status of the notification to read
	 * @ params: notification_id
	 * @ Table: wo_notifications
	 */
	public function status_post() {
		try {
			$notification_ids = json_decode ( $this->post ( 'notification_ids' ) );
			if (empty ( $notification_ids )) {
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => 'Please send notification_id' 
				);
			} else {
				$message = $this->notification->changeNotificationStatus ( $notification_ids );
			}
		} catch ( Exception $ex ) {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
		}
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
}
