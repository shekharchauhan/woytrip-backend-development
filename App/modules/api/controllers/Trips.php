<?php

/**
 * Created by PhpStorm.
 * User: Sanjay Yadav<yadav.sanjay@orangemantra.in>
 * Date: 08/10/2016
 * Time: 11:28 AM
 */
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
class Trips extends REST_Controller {
	function __construct() {
		// Construct the parent class
		parent::__construct ();
		$this->load->model ( 'trip_model', 'trip' );
		$this->load->model ( 'activity_model', 'activity' );
		$this->load->helper ( 'string' );
	}
	
	/*
	 * To get All the Trips of a User Per page 10
	 * @params: user_id , type ( user / friends)
	 * @Table: wo_trips
	 * @Author: Gaurav Ranjan
	 */
	public function index_post() {
		try {
			$user_id = $this->post ( 'user_id' );
			$type = ($this->post ( 'type' )) ? $this->post ( 'type' ) : '';
			$page = $this->input->post ( 'page' );
			if (isset ( $page )) {
				$pageno = $page;
			} 

			else {
				$pageno = '1';
			}
			$start_from = ($pageno - 1) * 10;
			if ($user_id == '') {
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => 'Please send user_id' 
				);
			} else {
				if ($type == 'friends') {
					$friends = $this->activity->getUserFriendsList ( $user_id ); // print_r($friends); die();
					if ($friends) {
						$result = $this->trip->getFriendsTrips ( $user_id,$friends, $start_from );
					}
				} else {
					$result = $this->trip->getTrips ( $user_id, $start_from );
				}
				if (! empty ( $result )) {
					$message = array (
							'status' => true,
							'response_code' => '1',
							'message' => 'Success',
							'data' => $result 
					);
				} else {
					$message = array (
							'status' => false,
							'response_code' => '0',
							'message' => 'No Friends Trip Available' 
					);
				}
			}
		} catch ( Exception $ex ) {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
		}
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
	
	/*
	 * Author : Gaurav Ranjan
	 * Date : 14-11-2016
	 * Description: This API will give you lists of your tripmates.
	 *
	 */
	public function getTripMates_post() {
		$TripID = $this->input->post ( 'trip_id' );
		
		if ($TripID) {
			$GetTrip = $this->trip->GetYourTripMembers ( $TripID );
			
			if (! empty ( $GetTrip )) {
				$message = array (
						'status' => true,
						'response_code' => '1',
						'message' => 'Success',
						'data' => $GetTrip 
				);
			} else {
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => 'No Members Found' 
				);
			}
		} 

		else {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => 'Please enter trip ID' 
			);
		}
		
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
	
	public function deleteTrip_post()
	{
		$TripID = $this->input->post('trip_id');
		if($TripID)
		{
			$delete = $this->trip->deleteTrip($TripID);
			
			if($delete == 1 )
			{
				$message = array (
						'status' => true,
						'response_code' => '1',
						'message' => 'Trip has been successfully deleted'
				);
			}
			
			else 
			{
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => 'Error!'
				);
			}
		}
		
		else {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => 'Please enter trip ID'
			);
		}
		
		$this->set_response ( $message, REST_Controller::HTTP_OK );
		
	}
}
