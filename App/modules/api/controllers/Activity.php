<?php

/**
 * Created by PhpStorm.
 * User: Gaurav Ranjan<ranjan.gaurav@orangemantra.in>
 * Date: 06/10/2016
 * Time: 11:28 AM
 */
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
class Activity extends REST_Controller {
	function __construct() {
		// Construct the parent class
		parent::__construct ();
		$this->load->model ( 'activity_model', 'activity' );
		$this->load->model ( 'user_model', 'user' );
		$this->load->helper ( 'string' );
		
		error_reporting ( 0 );
	}
	
	/*
	 * To get All the activities of a User
	 * @params: user_id (Of Whom you want to see the activity) ,activity_id ( to get details of particular Activity ), login_user_id( User who is logged in)
	 * @Table: wo_activities
	 */
	public function index_post() {
		// echo 123; die();
		try {
			$user_id = ( int ) $this->post ( 'user_id' );
			$page = $this->input->post ( 'page' );
			$activity_id = ( int ) $this->post ( 'activity_id' );
			$logged_in_user = ( int ) $this->input->post ( 'login_user_id' );
			if (isset ( $page )) {
				$pageno = $page;
			} 

			else {
				$pageno = '1';
			}
			$start_from = ($pageno - 1) * 10;
			if ($user_id == '' || $logged_in_user == 0) {
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => 'Please send user_id and login_user_id' 
				);
			} else {
				$result = $this->activity->getActivities ( $user_id, $start_from );
				if (! empty ( $result )) {
					if ($activity_id == 0) {
						$comments = $this->activity->getActivityComments ();
					} else {
						$comments = $this->activity->getSingleActivityComment ( $activity_id );
					}
					
					$message = array (
							'status' => true,
							'response_code' => '1',
							'message' => 'Success',
							'data' => $result,
							'comments' => $comments 
					);
				} else {
					$message = array (
							'status' => false,
							'response_code' => '0',
							'message' => 'No Activity Available' 
					);
				}
			}
		} catch ( Exception $ex ) {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
		}
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
	
	/*
	 * To Like an Activity
	 * @params: resource_id, poster_id
	 * @Table: wo_activity_likes
	 */
	public function like_post() {
		$data = array (
				'resource_id' => $this->post ( 'resource_id' ),
				'poster_id' => $this->post ( 'poster_id' ) 
		);
		$addLike = $this->activity->addLike ( $data );
		if ($addLike ['status'] == true) {
			$incrementPost = $this->activity->incrementLike ( $data );
			if ($incrementPost ['status']) {
				$addLike ['total'] = $incrementPost ['total_likes'];
			}
			$userInfo = $this->user->getUserInfoById ( $this->input->post ( 'poster_id' ) );
			$notification = array (
					'title' => $userInfo->name . ' likes your post',
					'message' => $this->input->post ( 'title' ),
					'subject_id' => $this->post ( 'resource_id' ),
					'subject_type' => 'like',
					'user_id' => $this->input->post ( 'poster_id' ),
					'user_name' => $userInfo->name,
					'user_pic' => $userInfo->user_pic 
			);
			// $this->sendNotification ( $incrementPost ['user_id'], $notification, false );
			$this->set_response ( $addLike, REST_Controller::HTTP_OK );
		} else {
			$this->set_response ( $addLike, REST_Controller::HTTP_OK );
		}
	}
	public function unlike_post() {
		$data = array (
				'resource_id' => $this->input->post ( 'resource_id' ),
				'like_id' => $this->input->post ( 'like_id' ) 
		);
		
		$removeLike = $this->activity->removeLike ( $data );
		if ($removeLike ['status'] == true) {
			$decPost = $this->activity->decrementLike ( $data );
			if ($decPost) {
				$removeLike ['total'] = $decPost;
			} 

			else {
				$removeLike ['total'] = 0;
			}
			$this->set_response ( $removeLike, REST_Controller::HTTP_OK );
		} else {
			$this->set_response ( $removeLike, REST_Controller::HTTP_OK );
		}
	}
	
	/*
	 * To Delete an Activity From wo_activity table based on activity_id.
	 * @params: activity_id
	 * @Table: wo_activities
	 */
	public function drop_post() {
		$id = ( int ) $this->post ( 'activity_id' );
		if ($id == 0) {
			$message = [ 
					'status' => false,
					'response_code' => '0',
					'message' => 'No Activity ID' 
			];
			$this->response ( $message, REST_Controller::HTTP_OK ); // BAD_REQUEST (400) being the HTTP response code
		}
		$result = $this->activity->delete ( $id );
		if ($result) {
			$message = [ 
					'status' => true,
					'response_code' => '1',
					'message' => 'Activity Deleted' 
			];
			
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		} else {
			$message = [ 
					'status' => false,
					'response_code' => '0',
					'message' => 'Bad request' 
			];
			
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		}
	}
	
	/*
	 * To Create an new Activity
	 */
	public function create_post() {
		
		// print_r($_POST); die();
		foreach ( (($this->post ())) as $key => $value ) {
			if (is_array ( $value )) {
				foreach ( $value as $key1 => $val )
					log_message ( 'info', 'dataarray=' . $key1 . ' =>' . $val );
			} else {
				log_message ( 'info', 'data=' . $key . ' =>' . $value );
			}
		}
		
		$userInfo = $this->user->getUserInfoById ( $this->input->post ( 'user_id' ) );
		$create = $this->activity->createActivity (); // print_r($create); die();
		//if ($this->input->post ( 'activity_seen' ) == 'public') {
			if ($create ['status'] == true) {
				$notification = array (
						'title' => $userInfo->name . ' has added a new post',
						'message' => $this->input->post ( 'title' ),
						'subject_id' => $create ['activity_id'],
						'subject_type' => 'new_post',
						'user_id' => $this->input->post ( 'user_id' ),
						'user_name' => $userInfo->name,
						'user_pic' => $userInfo->user_pic 
				);
				
				if ($this->input->post ( 'activity_type' ) == 'trip') {
					$mates = $this->input->post ( 'members' );
					$mates = json_decode ( $mates );
				}
				
				if ($this->input->post ( 'activity_type' ) == 'wall') {
					$GroupIds = $this->input->post ( 'group_id' );
					$GroupIds = json_decode ( $GroupIds );
					$mates = $this->activity->getAllGroupMembers ( $GroupIds );
					$mates = array_column ( $mates, 'user_id' );
				}
				$this->sendNotification ( '', $notification, $mates );
			}
			/*
			 * $message = [
			 * 'status' => true,
			 * 'response_code' => '1',
			 * 'message' => 'Activity Added Successfully'
			 * ];
			 */
			$message = $create;
// 		} else {
// 			$message = $create;
// 		}
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
	
	/*
	 * Function To Send an Notification
	 */
	public function sendNotification($userId = '', $data = array(), $mates) {
		if ($mates) {
			// $friends = $this->activity->getTripFriendsList ( $mates );
			$androidDeviceIds = getUserDeviceIDs ( $mates ); // print_r($androidDeviceIds); die();
		}
		$notificationData = $data;
		if (! empty ( $androidDeviceIds )) :
			androidNotification ( $androidDeviceIds, $notificationData );
		
		
		
		
		
        endif;
	}
	
	/*
	 * Author: Gaurav Ranjan
	 *
	 * Date : 11-11-2016
	 *
	 * Description : To Stop the trip
	 *
	 */
	public function endTrip_post() {
		$TripID = $this->input->post ( 'trip_id' );
		$UserID = $this->input->post ( 'user_id' );
		
		$EndTrip = $this->activity->EndYourTrip ( $TripID, $UserID );
		
		if ($EndTrip) {
			$this->set_response ( $EndTrip, REST_Controller::HTTP_OK );
		}
	}
}
